package components

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/23/12
 * Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
object Math {
  def rand(min: Int, max: Int): Int = {
    import scala.math;
    val result = math.floor(math.random * (max - min + 1)) + min;
    result.toInt;
  }
}
