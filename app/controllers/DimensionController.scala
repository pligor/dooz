package controllers

import play.api.mvc.{SimpleResult, AnyContent, Action, Controller}
import play.api.libs.json.{JsString, JsNumber, JsObject}
import play.api.Logger
import play.api.db.DB
import anorm._;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/21/12
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
object DimensionController extends Controller {
  def view(dimensionId: Int) = Action {
    val jsonResponse = new JsObject(Seq(
      ("id", JsNumber(dimensionId)),
      ("roomName", JsString("First and Only Room"))
    ));
    Ok(jsonResponse); //it gets itself that this is a json response
  }

  /**
   * REMEMBER: sto GET den mporoume na steiloume dedomena sto body tou request
   * @param dimensionId
   * @return
   */
  def retrieveUsersPosition(dimensionId: Int, username: String) = Action {
    implicit request =>

      val (user, pass) = try {
        (request.session("user"), request.session("pass"))
      } catch {
        case e: NoSuchElementException => (null, null);
      }

      if ((user, pass) ==(null, null)) {
        BadRequest("invalid username or password");
      }
      else {
        import play.api.Play.current;
        var (x, y) = (-1, -1);
        DB.withConnection {
          implicit connection =>
            val sqlString = "SELECT x,y\n" +
              "FROM dimension_position\n" +
              "WHERE user_id = (\n" +
              "\tSELECT id\n" +
              "\tFROM user\n" +
              "\tWHERE name = {name}\n)";

            val coordsRow = SQL(sqlString).on(
              "name" -> username
            )().head;

            x = coordsRow[Int]("x");
            y = coordsRow[Int]("y");
        }

        val positionResponse = new JsObject(Seq(
          ("x", JsNumber(x)),
          ("y", JsNumber(y))
        ));
        Ok(positionResponse);
      }
  }
}
