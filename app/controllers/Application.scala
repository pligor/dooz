package controllers

import play.api._

import data.Form;
import data.Forms._;

import db.DB
import mvc._
import anorm._
import models._;

object Application extends Controller {

  //TODO before action na orizetai to layout

  def frontpage = Action {
    val stylesheets = List[String](
      "common",
      "frontpage"
    );

    val hybridForm = Form(
      tuple(
        "nickname" -> text,
        "email" -> text,
        "password" -> text
      )
    );

    //TODO check form for errors

    val content = views.html.frontpage(hybridForm)(stylesheets);
    Ok(content).withNewSession; //clear session
  }

  def hybridSubmitted = Action(parse.urlFormEncoded) {
    request =>
      val formMap = request.body;

      val isRegistering = formMap("password").head.isEmpty; //List() is not empty, but has an empty string (if on right case)

      import play.api.Play.current;
      if (isRegistering) {
        DB.withConnection {
          implicit c =>
            val myMap = formMap - "password";

            val keys = myMap.keys;
            val columns = keys.mkString(",");
            val placeholders = keys.map("{" + _ + "}").mkString(",");

            val myMapNoLists = myMap.map(x => x._1 -> x._2.head);
            val params: Seq[(Any, ParameterValue[_])] = myMapNoLists.map(x => x._1 -> toParameterValue(x._2)).toSeq;
            val table_name = "registration_request";
            val sqlString = "INSERT INTO " + table_name + "(" + columns + ") VALUES(" + placeholders + ")";
            //Logger info sqlString;
            SQL(sqlString).on(params: _*).executeInsert();

          //TODO check for errors
        }
        Redirect(routes.Application.registrationResult());
      }
      else {
        val dimensionId = 1; //TODO gia otan tha exoume polla dimensions

        val formName = formMap("nickname").head;
        val formMail = formMap("email").head;
        val formPass = formMap("password").head;

        var isRegistered = false;

        DB.withConnection {
          implicit c =>
            val sqlString = "SELECT user_id,name,mail,pass FROM user,email_account WHERE user.id = user_id" + "\n" +
              "AND name={name} AND mail = {mail} AND pass = {pass}";
            val credsQuery = SQL(sqlString).on(
              "name" -> formName,
              "mail" -> formMail,
              "pass" -> formPass
            )();

            val credsRow = if (!credsQuery.isEmpty) credsQuery.head else null;

            isRegistered = if (credsQuery.isEmpty) {
              false;
            }
            else {
              val dbName = credsRow[String]("name");
              val dbMail = credsRow[String]("mail");
              val dbPass = credsRow[String]("pass");

              (formName == dbName) && (formMail == dbMail) && (formPass == dbPass);
            }

            if (isRegistered) {

              val userId = credsRow[Int]("user_id");

              val sqlString = "SELECT COUNT(*) AS is_positioned FROM dimension_position" + "\n" +
                "WHERE dimension_id = {dimension_id} AND user_id = {user_id}";

              val countRow = SQL(sqlString).on(
                "dimension_id" -> dimensionId,
                "user_id" -> userId
              )().head;

              val isPositioned = countRow[Long]("is_positioned") == 1;
              //              Logger info userId.toString;
              //              Logger info "positioned:" + isPositioned.toString;
              if (!isPositioned) {
                //TODO argotera tha dialegei o xristis tin thesi tou

                val dimsRow = SQL("SELECT width, height FROM dimension WHERE id = {dimension_id}").on(
                  "dimension_id" -> dimensionId
                )().head;

                val width = dimsRow[Int]("width");
                val height = dimsRow[Int]("height");

                val coords = Dimension.newCoords(width, height);
                val sqlString = "INSERT INTO dimension_position(dimension_id,user_id,x,y)" + "\n" +
                  "VALUES({dimension_id},{user_id},{x},{y})";

                SQL(sqlString).on(
                  "dimension_id" -> dimensionId,
                  "user_id" -> userId,
                  "x" -> coords._1,
                  "y" -> coords._2
                ).executeInsert();
                Logger
              }
            }
        }

        if (isRegistered) {
          Redirect(routes.Application.interact(dimensionId)).withSession(
            request.session + ("user" -> formName) + ("pass" -> formPass)
          );
        }
        else {
          Redirect(routes.Application.notRegistered());
        }
        //checks match of email, username and password
      }

    //def echo(args: (Any,ParameterValue[_])*) = for (arg <- args) { Logger info "hello"};
    //val params: Seq[(Any, ParameterValue[_])] = Seq("col1" -> "sss", "col2" -> "ssss");
    //echo(params: _*)
    //val res = keys + "\n" + values;

    // val res = "";
    //Ok(res toString)
  }

  def notRegistered = Action {
    val content = "Name or Email or Password are incorrect" + "\n" +
      "Please go back, and try again";
    Ok(content);
  }

  def registrationResult = Action {
    val content = "Later you will receive an email with your registration information!" + "\n" +
      "Thank you for your registration request!";
    Ok(content);
  }

  def admin(enable: Boolean) = Action {
    request =>
      val redirect = Redirect(routes.Application.interact(1));
      if (enable) {
        redirect.withSession(request.session +("uid", "1984")); //also keeping other session values
      }
      else {
        redirect.withNewSession;
      }
  }

  def interact(dimensionId: Int) = Action {
    request =>

    //remember I don't include php.full.js
      val admin = isAdmin(request);

      val jqueryPlugins = List[String](
        "jquery.tmpl",
        "jquery.tooltip"
      );

      //BACKBONE
      val backbone = List[String](
        "underscore",
        "json2",
        "backbone"
      );

      val jslibs = jqueryPlugins ++ backbone ++ List[String](
        "strophe",
        "strophe.muc",
        "sylvester.src",
        "raphael"
      );

      val myjslibs = List[String](
        "prototypes",
        "generic",
        "XmlHelper"
      );

      //be aware of the order here
      val javascripts: List[String] = jslibs ++ myjslibs ++
        //(if (admin) List[String]("peek") else List.empty) ++
        List[String]("peek") ++
        List[String](
          "sochat",
          "mainstream",
          "Spring",
          "Node",
          "Canvas",
          //"MucRoster",
          "MessagePanes",
          "Dimension"
        );

      val stylesheets = List[String](
        "common",
        "interact",
        "peek",
        "jquery-ui-1.8.23.custom",
        "jquery.tooltip"
      );

      val (user, pass) = try {
        (request.session("user"), request.session("pass"))
      } catch {
        case e: NoSuchElementException => (null, null);
      }

      if ((user, pass) ==(null, null)) {
        Redirect(routes.Application.frontpage());
      }
      else {
        val content = views.html.interact(user, pass, dimensionId, admin)(stylesheets, javascripts);
        Ok(content);
      }
  }

  def isAdmin(request: RequestHeader) = try {
    request.session("uid") == "1984";
  }
  catch {
    case e: NoSuchElementException => false;
  }

  def testing = Action {
    //BACKBONE
    val backbone = List[String](
      "underscore",
      "json2",
      "backbone"
    );

    val javascripts = List[String](
      "prototypes"
    ) ++ backbone ++ List[String](
      "Scrollable"
    );

    //val mycoords = Dimension.newCoords(4, 4);
    //Logger info mycoords.toString();

    //Ok("check logger info");
    Ok(views.html.testing(stylesheets = List.empty, javascripts = javascripts));
  }

  /**
   * AUTO THA PEI dynamic!! pernas thn function ws orisma
   * @return
   */
  def index = Action {
    Ok(views.html.index("Your new application is ready.", views.html.mytmpl.apply));
  }
}