Testing = {
	mycollection: null
};

AppRouter = Backbone.Router.extend({
	/*routes: {	//order here matters
		'/posts/:id': 'getPost', // <a href="http://example.com/#/posts/121">Example</a>
		'/download/*path': 'downloadFile',
		"/:route/:action": "loadView",
		'*actions': 'defaultRoute'
	},
	defaultRoute: function( actions ) {
		//console.log(actions); //the variable passed in matches the variable in the route definition "actions"
	},
	getPost: function(id) { console.log('receiving post with id: '+id); },
	downloadFile: function(file) { console.log('downloading file: '+file); },
	loadView: function(view,action) { console.log('loading: '+ view +' with action: '+ action); }*/
	routes: {	//order here matters, REMEMBER: preleading slash '/' has been deprecated
		'model/create': 'createRoute',
		'model/retrieve/:id': 'retrieveRoute',
		'model/:action/:id': 'updateRoute',
		'delete/:id': 'deleteRoute',
		'index/*action': 'indexRoute'
	},
	createRoute: function() {
		//params here are undefined which is wanted and expected :)
		console.log('create model');
		Testing.mycollection.create({
			expire: 7000,
			data: 'nully'
		},{
			wait: true
		});
	},
	retrieveRoute: function(id) {
		console.log('retrieve model with id: '+id);
		var model = Testing.mycollection.get(id);
		model.fetch();
	},
	updateRoute: function(action,id) {
		console.log(action +' model with id: '+id);
		var model = Testing.mycollection.get(id);
		model.save({
			expire: 425
		},{
			wait: true
		});
	},
	deleteRoute: function(id) {
		console.log('delete model with id: '+id);
		var model = Testing.mycollection.get(id);
		model.destroy({
			wait: true
		});
	},
	indexRoute: function(actions) {
		console.log('get all models: '+actions);
		Testing.mycollection.fetch();
	}
});

YiiSession = Backbone.Model.extend({
	defaults: {		//defaults are necessary so you don't have any missing models
		expire: 0,
		data: ''
	},
	initialize: function() {
		
	}
});

YiiSessions = Backbone.Collection.extend({
	model: YiiSession,
	url: '/dooz/index.php?r=restapi&class=YiiSession',
	initialize: function() {
		
	}
});