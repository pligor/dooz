var Generic = {
	isAtBottom:function (element) {
		//TODO perhaps we must do a check if it is a scrollable element
		//total height - console visible height <=> max scroll top position
		if (element instanceof jQuery) {
			return element.scrollTop() >= (element[0].scrollHeight - element.height());
		}
		else {
			return element.scrollTop >= (element.scrollHeight - element.clientHeight);
		}

	},
	setToMaxScroll:function (element) {
		//total height - console visible height <=> max scroll top position
		if (element instanceof jQuery) {
			element.scrollTop(element[0].scrollHeight - element.height());
		}
		else {
			element.scrollTop = element.scrollHeight - element.clientHeight;
		}
	},

	cloneObject:function (obj) {
		if (!jQuery) {
			throw 'please load jquery before using this function';
		}

		return jQuery.extend(true, {}, obj);
	}
}

/**
 * DEN LEITOURGEI H MALAKIA
 * @param q
 * @param s
 * @return {*}
 */
function $_GET(q, s) {
	s = s || window.location.search;
	var re = new RegExp('&' + q + '=([^&]*)', 'i');
	return (s = s.replace(/^\?/, '&').match(re)) ? s = s[1] : s = '';
}

/*
 Object.prototype.getName = function() { //get the real name of the object
 var funcNameRegex = /function (.{1,})\(/;
 var results = (funcNameRegex).exec((this).constructor.toString());
 return (results && results.length > 1) ? results[1] : "";
 };
 */

function grabData(container, field_type) {
	//return container.find("input").serialize();
	//return container.find("input").serializeArray();
	//var container = $("#"+row_id);
	var inputs = container.find(field_type);
	var values = {};
	inputs.each(function () {
		values[this.name] = $(this).val();
	});
	return values;
}

function grabInputData(container) {
	//return container.find("input").serialize();
	//return container.find("input").serializeArray();
	//var container = $("#"+row_id);
	var inputs = container.find('input');
	var values = {};
	inputs.each(function () {
		values[this.name] = $(this).val();
	});
	return values;
}

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns Object obj3 a new object based on obj1 and obj2
 */
function merge_options(obj1, obj2) {
	var obj3 = {};
	for (var attrname in obj1) {
		obj3[attrname] = obj1[attrname];
	}
	for (var attrname in obj2) {
		obj3[attrname] = obj2[attrname];
	}
	return obj3;
}


function initMultiDimArray(x, y) {
	var twoDarray = new Array(x);
	for (var i = 0; i < x; i++) {
		twoDarray[i] = new Array(y);
	}
	return twoDarray;
}

function mt_rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

Array.prototype.avg = function () {
	var av = 0;
	var cnt = 0;
	var len = this.length;
	for (var i = 0; i < len; i++) {
		var e = +this[i];
		if (!e && this[i] !== 0 && this[i] !== '0') e--;
		if (this[i] == e) {
			av += e;
			cnt++;
		}
	}
	return av / cnt;
}


