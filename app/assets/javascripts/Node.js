/**
 * Created by pligor
 */

var NodeModel;
var NodeView;
var NodeCollection;
var NodeCollectionView;

/**
 * CONVENTION: parent of the NodeModel is the Canvas. NodeCollection only stores the parent (canvas) so we can add
 * models to the collection and do not have to pass the parent as a parameter each time. All models have the same
 * parent.
 * Parent of the NodeView is actually the CanvasView which owns the Raphael JS paper so these views can
 * draw on paper.the NodeCollectionView stores the parent for the same reason as above
 */
$(function () {
	NodeModel = Backbone.Model.extend({
		defaults:{
			parent:null,
			position:Vector.Zero(2),
			velocity:Vector.Zero(2),
			charge:500,
			mass:1,
			springs:null
		},
		initialize:function () {
			var springCollection = new SpringCollection();
			springCollection.parentNodeModel = this;
			this.set('springs', springCollection);

			_.bindAll(this, 'disconnectWith', 'normalizeCoords', 'equalId', 'distance');

			this.on('remove', this.removeSprings, this);
		},
		removeSprings:function () {
			this.get('springs').clear();
		},
		set:function (key, value, options) { //http://jstarrdewar.com/blog/2012/07/20/the-correct-way-to-override-concrete-backbone-methods

			var isKeyObject = _.isObject(key);

			var settingPosition = isKeyObject ? key.hasOwnProperty('position') : (key == 'position');

			var newKey = key;
			var newValue = value;
			var newOptions = options;

			if (settingPosition) {
				var newPosition = isKeyObject ? key.position : value;

				var parent = this.get('parent') || key.parent;
				var newActualPosition = this.normalizeCoords(newPosition, parent.get('width'), parent.get('height'));

				newKey = isKeyObject ? jQuery.extend(true, {}, key, {position:newActualPosition}) : key;

				newValue = isKeyObject ? value : newActualPosition;

				newOptions = isKeyObject ? null : options;
			}

			//by implementation third parameter is ignored when key is object
			var result = Backbone.Model.prototype.set.call(this, newKey, newValue, newOptions);

			if (settingPosition) {
				if (this.get('springs')) this.get('springs').trigger('node_moved');
			}

			return result;
		},
		normalizeCoords:function (position, width, height) {
			var velocity = this.get('velocity');

			if (position.e(1) > width) {
				position.setElements([width, position.e(2)]);
				if (velocity) velocity.setElements([0, velocity.e(2)]);
			}
			else if (position.e(1) < 0) {
				position.setElements([0, position.e(2)]);
				if (velocity) velocity.setElements([0, velocity.e(2)]);
			}

			if (position.e(2) > height) {
				position.setElements([position.e(1), height]);
				if (velocity) velocity.setElements([velocity.e(1), 0]);
			}
			else if (position.e(2) < 0) {
				position.setElements([position.e(1), 0]);
				if (velocity) velocity.setElements([velocity.e(1), 0]);
			}

			if (velocity) this.set('velocity', velocity);

			return position;
		},
		connectWith:function (node) {
			if (!node) {
				throw 'node does not exist';
			}

			if (this === node) {
				throw 'invalid to self-connect!';
			}

			try {
				var obj = {
					start_node:this,
					end_node:node
				};
				this.get('springs').add(obj);

				var id = this.get('springs').generateID(obj);
				var spring = this.get('springs').get(id);
				node.get('springs').add(spring);

				this.trigger('spring_attached', spring);
			}
			catch (e) {
				console.log(e);
			}
		},
		disconnectWith:function (node) {
			if (!node) {
				throw 'node does not exist';
			}
			if (this === node) {
				throw 'invalid to self-disconnect!';
			}

			var springs = this.get('springs');
			var spring = springs.get(new Array(this.id, node.id)) || springs.get(new Array(node.id, this.id));
			if (spring != undefined) {
				spring.trigger('remove');
			}
		},
		equalId:function (node) {
			return (node.id == this.id);
		},
		distance:function (node) {
			return this.get('position').subtract(node.get('position')).modulus();
		}
	});

	NodeView = Backbone.View.extend({
		parent:null,
		shape:null,
		radius:20,
		stroke_width:2,
		color:'lightblue', //css colors are ok ;)

		events:{    //events only have to do with the dom
			//'click #send_button': 'onSendButtonClick'
		},
		initialize:function () {
			_.bindAll(this, 'makeInteractive');

			this.model.on('change:position', this.positionChanged, this);
			this.model.on('change:velocity', this.velocityChanged, this);

			this.model.on('spring_attached', this.springAttached, this);

			this.model.on('remove', this.removed, this);
		},
		positionChanged:function () {
			//both methods (via params or via object) are ok
			this.shape.attr('cx', this.model.get('position').e(1));
			this.shape.attr({cy:this.model.get('position').e(2)});
		},
		velocityChanged:function () {
			console.log('velocity has changed');
		},
		render:function () {
			this.shape = this.parent.paper.circle(
				this.model.get('position').e(1),
				this.model.get('position').e(2),
				this.radius
			);

			this.shape.attr('stroke-width', this.stroke_width);
			this.shape.attr('fill', this.color);

			return this;
		},
		makeInteractive:function () {    //[0] of a shape always points to the DOM object
			/*this.shape.click(function() { console.log('clicked upon'); });
			 $(this.shape[0]).on('click',function(){ console.log('mas clickare!'); });*/

			$(this.shape[0]).tooltip({
				bodyHandler:_.bind(function () {
					return this.model.id;	//can be real html
				}, this),
				showURL:false
			});

			/*this.shape.click(_.bind(function (event, data) {
			 console.log(this.model);
			 }, this));*/

			this.shape.dblclick(_.bind(function (event, data) {
				event.preventDefault();

				var username = this.model.id;
				this.model.get('parent').trigger('directMessage', {username:username});
			}, this));
		},
		springAttached:function (spring) {
			var springView = new SpringView({
				model:spring
			});
			springView.parent = this.parent;

			springView.render();
		},
		removed:function () {
			this.shape.remove();
		}
	});

	NodeCollection = Backbone.Collection.extend({
		model:NodeModel,
		parent:null,
		autoincrement:1,
		initialize:function () {
			_.bindAll(this, 'generateID', 'getSafeCoords', 'shuffleNodes');	//http://stackoverflow.com/questions/7887595/underscore-bindall-preserving-the-this-context
		},
		add:function (obj) {
			var json;
			try {
				json = obj.toJSON();
			}
			catch (e) {
				json = obj;
			}

			if (!json.id) {
				json.id = this.generateID();
			}
			json.parent = this.parent;
			json.position = this.getSafeCoords(json.position);
			//console.log(json.position);
			Backbone.Collection.prototype.add.call(this, json);	//call parent's "add" method

			return this.get(json.id); //broken backbone: normally add method returns nothing
		},
		generateID:function () {
			throw 'we should use the username as the id, normally this must not be called';
			return this.autoincrement++;
		},
		maxSprings:function () {
			return this.length * (this.length - 1) / 2;   //meta apo anagoges athrismatos arithmitikis proodou
		},
		fillNodes:function (num) {
			for (var i = 0; i < num; i++) {
				this.add({
					parent:this.parent,
					position:this.parent.getRandomPosition()
				});
			}
		},
		getSafeCoords:function (position) { //making sure this node does not have exactly the same position as another node
			//TODO test this function
			/*while (true) {
			 if (this.all(function (model) {
			 return !position.eql(model.get('position'));
			 })) {
			 break;
			 }
			 position = this.parent.getRandomPosition();
			 }*/
			return position;
		},
		shuffleNodes:function () {
			this.each(function (model, index, list) {
				var position = this.parent.getRandomPosition();
				position = this.getSafeCoords(position);
				model.set('position', position);
			}, this);
		}
	});

	NodeCollectionView = Backbone.View.extend({
		parent:null,
		initialize:function () {
			//_.bindAll(this, 'add_node', 'remove_node');
			this.collection.on('add', this.add_node, this);
			this.collection.on('remove', this.remove_node, this);
		},
		add_node:function (model) {
			var modelView = new NodeView({model:model});
			modelView.parent = this.parent;
			modelView.render().makeInteractive();
		},
		remove_node:function (model) {
			//console.log('please remove node ' + model.id);
		},
		render:function () {
			return this;
		}
	});
});