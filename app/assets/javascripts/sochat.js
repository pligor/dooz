$(function () {
	Strophe.addConnectionPlugin('sochat', {
		_connection:null,
		init:function (connection) {
			this._connection = connection;
			Strophe.addNamespace('SOCHAT', 'http://sochat.com/protocol');
		},
		/*
		 * EXAMPLE announceChat
		 *<message id="3378" xmlns="jabber:client" to="admin@127.0.0.1/ed1d792c" from="room.name@conference.127.0.0.1/boy" type="groupchat">
		 *	<body>some text body is ALWAYS necessary in messages</body>
		 *	<chatfrom xmlns="http://sochat.com/protocol">username_sender</chatfrom>
		 *	<chatto xmlns="http://sochat.com/protocol">username_receiver</chatto>
		 *	<type xmlns="http://sochat.com/protocol">connection</type>
		 *</message>
		 */
		announceChat:function (room, sender, receiver, conn_type) {
			var msgid = this._connection.getUniqueId();
			var msg = $msg({
				to:room,
				from:this._connection.jid,
				type:'groupchat',
				id:msgid
			});

			msg.c('body').t(conn_type + ' from ' + sender + ' to ' + receiver);

			msg.up();

			msg.c('chatfrom', {
				xmlns:Strophe.NS.SOCHAT
			}).t(sender);

			msg.up();

			msg.c('chatto', {
				xmlns:Strophe.NS.SOCHAT
			}).t(receiver);

			msg.up();

			msg.c('type', {
				xmlns:Strophe.NS.SOCHAT
			}).t(conn_type);

			this._connection.send(msg);
			return msgid;
		},
		isAnnounceChat:function (msg) {
			return (msg.children('chatfrom').length === 1) &&
				(msg.children('chatto').length === 1) &&
				(msg.children('type').length === 1);
		}
	});
});
