var Mainstream = {
	BOSH_SERVICE:'/xmpp-httpbind',
	//XMPP_DOMAIN:'127.0.0.1',
	//XMPP_DOMAIN:'5.79.16.68',
	XMPP_DOMAIN:null,

	GROUP_CHAT_SERVICE:function () {
		return 'conference.' + Mainstream.XMPP_DOMAIN;
	},

	conn:null,
	start_time:new Array(), //msec[]
	ping_pong:3,
	lags:new Array(), //msec[]

	send_ping:function (destination) {
		var id = --Mainstream.ping_pong;
		var ping = $iq({
			to:destination,
			type:'get',
			id:id
		}).c('ping', {xmlns:'urn:xmpp:ping'});

		Mainstream.start_time[id] = (new Date()).getTime();

		Mainstream.conn.send(ping);
	},
	handle_pong:function (iq) {
		var id = $(iq).attr('id');
		var time = (new Date()).getTime() - Mainstream.start_time[id];
		Mainstream.lags[id] = time;
		//console.log(Mainstream.lags);

		if (Mainstream.ping_pong >= 0) { //serially
			Mainstream.send_ping(Mainstream.XMPP_DOMAIN);
		}
		else {
			var avg_lag = Mainstream.lags.avg();
			console.log('average lag: ' + avg_lag);
			console.log(Mainstream.lags);

			Mainstream.sendMessage('girl', avg_lag.toString());

			return false;	//false means the handler function will NOT be executed again
		}

		return true;
	},
	sendMessage:function (to, message) {
		var msg = $msg({to:to + '@' + Mainstream.XMPP_DOMAIN, type:'chat'}).c('body').t(message);
		Mainstream.conn.send(msg);
	},

	getNode:function () {
		return Strophe.getNodeFromJid(Mainstream.conn.jid); //boy@127.0.0.1/4a1b1d69 -> boy@127.0.0.1
	},

	/**
	 * @todo there is no handler for this function yet
	 * NORMALLY THIS MUST BE EXECUTED FROM THE SERVER -> CLIENT TO GATHER CLIENT STATISTICS
	 */
	askVersion:function (to) {
		var iq = $iq({
			type:'get',
			id:Mainstream.conn.getUniqueId(),
			to:to
		}).c('query', {xmlns:'jabber:iq:version'});
		Mainstream.conn.send(iq);
	}
};

$(Mainstream).on('connected', function (event, data) {
	var conn_ok = 'connection established with full jid: ' + Mainstream.conn.jid;
	console.log(conn_ok);

	//disabled ping for now
	//Mainstream.conn.addHandler(Mainstream.handle_pong, null, 'iq');
	//var domain = Strophe.getDomainFromJid(Mainstream.conn.jid);
	//Mainstream.send_ping(domain);
});

$(Mainstream).on('disconnected', function (event, data) {
	console.log('connection terminated');
	Mainstream.conn = null;
});

$(document).on('connect', function (event, data) {
	var jid = data.user + '@' + Mainstream.XMPP_DOMAIN;
	var pass = data.pass;

	Mainstream.conn = new Strophe.Connection(Mainstream.BOSH_SERVICE);

	Mainstream.conn.connect(jid, pass, function (status) {
		if (status === Strophe.Status.CONNECTED) {
			$(Mainstream).trigger('connected');

			//console.log("pame gia elegxo");
			if (typeof(Peek) != 'undefined') {
				//$(Peek).trigger('connected');
				Peek.trigger('connected');
				//console.log("perasame apo edw mesa?");
			}

			if (typeof(Dimension) != 'undefined') {
				Dimension.trigger('connected');
			}
		}
		else if (status === Strophe.Status.DISCONNECTED) {
			$(Mainstream).trigger('disconnected');
		}
	});

	$(window).unload(function () {
		Mainstream.conn.disconnect();
	});

	//console.log("eisai edw?");
	if (typeof(Peek) != 'undefined') {
		//console.log("eisai mesa?");
		Peek.trigger('connection_set', Mainstream.conn);
	}
});
