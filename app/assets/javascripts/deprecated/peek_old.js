var Peek_dummy_postfix = {
    show_traffic:function (body, type) {
        var styling = null;
        if (type == 'incoming') {
            styling = 'style="background-color: #DDD;"';
        }

        if (body.childNodes.length > 0) {
            var console = $('#peek_console');
            var is_at_bottom = console.scrollTop >= (console.scrollHeight - console.clientHeight);  //total height - console visible height <=> max scroll top position

            $(body.childNodes).each(function () {
                $('#peek_console').append('<div ' + styling + '>' +
                    //Peek.xml2html(Strophe.serialize(this))
                    Peek.pretty_xml(this)
                    + '</div>');
            });

            if (is_at_bottom) {
                console.scrollTop = console.scrollHeight - console.clientHeight; //set to max scroll top position
            }
        }
    },
    pretty_xml:function (xml, level) {
        var result = [];
        if (!level) {
            level = 0;
        }

        result.push('<div class="xml_level' + level + '">');
        result.push('<span class="xml_punc">&lt;</span>');
        result.push('<span class="xml_tag">');
        result.push(xml.tagName);
        result.push('</span>');

        //attributes
        var attrs = xml.attributes;
        var attr_lead = [];
        for (var i = 0; i < xml.tagName.length + 1; i++) {
            attr_lead.push('&nbsp;');
        }
        attr_lead = attr_lead.join('');

        for (var i = 0; i < attrs.length; i++) {
            result.push(' <span class="xml_aname">');
            result.push(attrs[i].nodeName);
            result.push('</span>');
            result.push('<span class="xml_punc">="</span>');
            result.push('<span class="xml_avalue">');
            result.push(attrs[i].nodeValue);
            result.push('</span>');
            result.push('<span class="xml_punc">"</span>');

            if (i !== (attrs.length - 1)) { //in case we are not at the last element
                result.push('</div>');
                result.push('<div class="xml_level' + level + "'>");
                result.push(attr_lead);
            }
        }

        if (xml.childNodes.length === 0) {   //last element
            result.push('<span class="xml_punc">/&gt;</span></div>');
        }
        else {
            result.push('<span class="xml_punc">&gt;</span></div>');

            //for each children
            $(xml.childNodes).each(function () {
                if (this.nodeType === 1) {
                    result.push(Peek.pretty_xml(this, level + 1));
                }
                else if (this.nodeType === 3) {
                    result.push('<div class="xml_text xml_level' + (level + 1) + '">');
                    result.push(this.nodeValue);
                    result.push('</div>');
                }
            });

            result.push('<div class="xml_level' + level + '">');
            result.push('<span class="xml_punc">&lt;/</span>');
            result.push('<span class="xml_tag">');
            result.push(xml.tagName);
            result.push('</span>');
            result.push('<span class="xml_punc">&gt;</span>');
            result.push('</div>');
        }

        return result.join('');
    },
    xml2html:function (s) {
        return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/, '&gt;');
    },
    text2xml:function (text) {
        var doc = null;
        if (window['DOMParser']) {
            var parser = new DOMParser();
            doc = parser.parseFromString(text, 'text/xml');
        }
        else if (window['ActiveXObject']) {
            var doc = new ActiveXObject("MSXML2.DOMDocument");
            doc.async = false;
            doc.loadXML(text);
        }
        else {
            throw {type:'Error from peek_old.js', message:'No DOMParser object found'};
        }

        var elem = doc.documentElement;
        if ($(elem).filter('parsererror').length > 0) {
            return null;
        }

        return elem;
    }
};


try {
    $(document).ready(function () {
        $(Peek).on('connected', function (event, data) {
            $('#peek_input').attr('disabled', false);
        });
        $(Peek).on('disconnected', function (event, data) {
            $('#peek_input').attr('disabled', true);
        });

        $(Peek).on('connection_set', function (event, data) {
            Mainstream.conn.xmlInput = function (body) {
                console.log("incoming!")
                Peek.show_traffic(body, 'incoming');
            };
            Mainstream.conn.xmlOutput = function (body) {
                console.log("outgoing!!")
                Peek.show_traffic(body, 'outgoing');
            };
        });

        $('#peek_input').keyup(function () {
            if ($(this).val() == '') {
                $('#send_button').attr('disabled', true);
            }
            else {
                $('#send_button').attr('disabled', false);
            }
            $(this).css({'background-color':'#fff'});
        });

        //*
        $('#send_button').click(function () {
            var textbox = $('#peek_input');
            var input = textbox.val();
            var error = true;
            if (input[0] === '<') {
                var xml = Peek.text2xml(input);
                if (xml) {
                    Mainstream.conn.send(xml);
                    error = false;
                }
            }
            else if (input[0] === '$') {
                try {
                    var builder = eval(input);
                    Mainstream.conn.send(builder);
                    error = false;
                }
                catch (e) {
                    error = true;	//pleonasmos
                }
            }

            if (error) {
                $('#peek_input').css({'background-color':'#aaa'});
            }
            else {
                textbox.trigger('keyup');	//we break the promise that we will not trigger predefined events
            }
        });
    });
} catch (e) {
    alert("An exception occurred in the script. Error name: " + e.name + ". Error message: " + e.message);
}
