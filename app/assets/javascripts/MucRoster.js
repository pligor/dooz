/**
 * Created by pligor
 */

var MucUser;
var MucUserView;
var MucRoster;
var MucRosterView;

$(document).ready(function () {
	MucUser = Backbone.Model.extend({
		defaults:{
			affiliation:'none', //http://xmpp.org/extensions/xep-0045.html#affil
			role:'participant', //http://xmpp.org/extensions/xep-0045.html#roles
			parentDimension:null
		},
		initialize:function () {

		}
	});

	MucUserView = Backbone.View.extend({
		tagName:'li',

		template:null,

		events:{
			'click a':'clicked'
		},

		initialize:function () {
			_.bindAll(this, 'removeItem', 'clicked');

			this.model.on('remove', this.removeItem);
			this.template = $('script#roster_user');
		},
		clicked:function (ev) {
			ev.preventDefault();

			var username = this.model.id;
			this.model.get('parentDimension').trigger('directMessage', {username:username});
		},
		render:function () {
			var html = this.template.tmpl(this.model.toJSON());
			this.$el.append(html);
			return this;
		},
		removeItem:function () {
			//console.log('remove now the li');
			this.remove();
		}
	});

	MucRoster = Backbone.Collection.extend({
		model:MucUser,
		parentDimension:null,
		initialize:function () {
			//_.bindAll(this);	//http://stackoverflow.com/questions/7887595/underscore-bindall-preserving-the-this-context
			this.on('add', function (model) {
				model.set('parentDimension', this.parentDimension);
			});
		},
		onUserInDimension: function(args) {
			this.add({
				id: args.username,
				affiliation: args.affiliation,
				role: args.role
			});
		},
		onUserOutOfDimension: function(args) {
			this.remove(this.get(args.username));
		}
	});

	MucRosterView = Backbone.View.extend({
		el:'ul#room_roster', //already exists in the dom

		initialize:function (parentDimension) {
			this.collection = new MucRoster();
			this.collection.parentDimension = parentDimension;
			this.collection.parentDimension.on('userInDimension',this.collection.onUserInDimension,this.collection);
			this.collection.parentDimension.on('userOutOfDimension',this.collection.onUserOutOfDimension,this.collection);

			//_.bindAll(this, 'add_user', 'remove_user');

			this.collection.on('add', this.add_user, this);
			this.collection.on('remove', this.remove_user, this);
		},

		add_user:function (model) {
			var userView = new MucUserView({model:model});
			this.$el.append(userView.render().$el);
		},

		remove_user:function (model) {
		},

		render:function () {
			return this;
		}
	});
});