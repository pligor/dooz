/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/19/12
 * Time: 6:40 PM
 * To change this template use File | Settings | File Templates.
 */

ScrollableModel = Backbone.Model.extend({
	defaults:{
		width:320,
		height:240,
		scrollbars:true,
		step:10,

		upContent:'up<br/>arrow',
		upWidth:50,

		downContent:'down<br/>arrow',
		downWidth:50,

		leftContent:'left<br/>arrow',
		leftHeight:50,

		rightContent:'right<br/>arrow',
		rightHeight:50
	},
	initialize:function () {

	}
});

ScrollableView = Backbone.View.extend({
	tag:'div',
	upArrow:null,
	leftArrow:null,
	downArrow:null,
	rightArrow:null,
	wrapper:null,
	directionRight:{
		space_avail:function (container, wrapper) {
			return container.width() - wrapper.width() - wrapper[this.func]();
		},
		sign:'+',
		func:'scrollLeft'
	},
	directionLeft:{
		space_avail:function (container, wrapper) {
			return wrapper[this.func]();
		},
		sign:'-',
		func:'scrollLeft'
	},
	directionUp:{
		space_avail:function (container, wrapper) {
			return wrapper[this.func]();
		},
		sign:'-',
		func:'scrollTop'
	},
	directionDown:{
		space_avail:function (container, wrapper) {
			return container.height() - wrapper.height() - wrapper[this.func]();
		},
		sign:'+',
		func:'scrollTop'
	},
	initialize:function () {
		_.bindAll(this, 'loop');
	},
	render:function () {
		var wrapper_html = $('<div style="position: absolute;left:0;top:0;' +
			'width: ' + this.model.get("width") + 'px;' +
			'height: ' + this.model.get("height") + 'px;' +
			'overflow: ' + ( this.model.get('scrollbars') ? 'scroll' : 'hidden' ) + ';"></div>');

		this.wrapper = this.$el.wrap(wrapper_html).parent();

		var overlay_html = $('<div style="width:320px;height: 240px;"></div>');
		var overlay = this.wrapper.wrap(overlay_html).parent();

		//console.log(overlay);

		//var upArrow = $('<div style="width: ' + this.model.get("upWidth") + 'px;margin: auto;">' +
		this.upArrow = $('<div style="z-index: 1;position: absolute;top:0;left: ' +
			( (this.model.get('width') / 2) - (this.model.get('upWidth') / 2) ) + 'px;">' +
			this.model.get('upContent')
			+ '</div>');

		overlay.prepend(this.upArrow);	//absolute position... so either prepend or append works the same ;)

		this.downArrow = $('<div style="z-index: 1;position: absolute;bottom:0;left: ' +
			( (this.model.get('width') / 2) - (this.model.get('downWidth') / 2) ) + 'px;">' +
			this.model.get('downContent')
			+ '</div>');

		overlay.prepend(this.downArrow);	//absolute position... so either prepend or append works the same ;)

		this.leftArrow = $('<div style="z-index: 1;position: absolute;left:0;top: ' +
			( (this.model.get('height') / 2) - (this.model.get('leftHeight') / 2) ) + 'px;">' +
			this.model.get('leftContent')
			+ '</div>');

		overlay.prepend(this.leftArrow);	//absolute position... so either prepend or append works the same ;)

		this.rightArrow = $('<div style="z-index: 1;position: absolute;right:0;top: ' +
			( (this.model.get('height') / 2) - (this.model.get('rightHeight') / 2) ) + 'px;">' +
			this.model.get('rightContent')
			+ '</div>');

		overlay.prepend(this.rightArrow);	//absolute position... so either prepend or append works the same ;)

		//koita edw malakia! gia na katalavei ti ginetai prepei prwta na valoume ta arrows kai meta! na orisoume
		//to position relative tou html!!

		overlay.css('position', 'relative');

		//SET EVENTS
		var stopit = _.bind(function () {
			this.wrapper.stop();
		}, this);

		this.rightArrow.hover(_.bind(function () {
			this.loop(this.directionRight, this.model.get('step'), false);
			return false;
		}, this), stopit);

		this.leftArrow.hover(_.bind(function () {
			this.loop(this.directionLeft, this.model.get('step'), false);
			return false;
		}, this), stopit);

		this.upArrow.hover(_.bind(function () {
			this.loop(this.directionUp, this.model.get('step'), false);
			return false;
		}, this), stopit);

		this.downArrow.hover(_.bind(function () {
			this.loop(this.directionDown, this.model.get('step'), false);
			return false;
		}, this), stopit);

		return this;
	},
	loop:function (direction, step, once) {
		var completeFunc = once ? _.bind(function () {
			this.wrapper.stop();
		}, this) : _.bind(function () {
			var space_avail = direction.space_avail(this.$el, this.wrapper);

			//console.log(space_avail < step);	//

			space_avail < step ? this.loop(direction, space_avail, true) : this.loop(direction, step, false);
		}, this);

		var anim = {};
		anim[direction.func] = direction.sign + '=' + step;

		if (direction.space_avail(this.$el, this.wrapper) != 0) {
			this.wrapper.stop().animate(anim, step * 10, 'linear', completeFunc);
		}
	}
});