/**
 * Created by pligor
 * CONVENTION: id is the target name (with whom I am chatting)
 */
var MessageView;
var MessagePane;
var MessagePaneView;
var MessagePanes;
var MessagePanesView;

$(function () {
	MessageView = Backbone.View.extend({
		tagName:'div',
		initialize:function () {
		},
		render:function (sender, message_body) {
			var gmt_string = (new Date()).toGMTString();	//@todo LATER USE timeago: http://timeago.yarp.com/
			this.$el.append(sender + ' says (' + gmt_string + '): ' + message_body);

			return this;
		}
	});

	/**
	 * CONVENTION: id is the target name (with whom I am chatting)
	 */
	MessagePane = Backbone.Model.extend({
		defaults:{
			parentDimension:null
		},
		initialize:function () {    //is called AFTER the attributes are being assigned
			//_.bindAll(this,'sendMessage');
		},
		sendMessage:function (msg) {
			Mainstream.conn.muc.message(
				this.get('parentDimension').getChatroomJid(), //room
				this.id, //nick, if nick is null, the type is implied 'groupchat'
				msg    //, //message
				//null, //html_message
				//'chat'	//type, if nick is present the 'chat' is already implied
			);
			/*
			 Mainstream.conn.muc.groupchat(
			 Participant.getChatroomJid(), //room
			 msg//, //message
			 //null, //html_message
			 );*/
		}
	});

	MessagePaneView = Backbone.View.extend({
		//as always the model is passed as a parameter to the constructor
		tagName:'div',

		template:null,

		events:{
			'click .send_button':'onSendButtonClick'
		},

		initialize:function () {
			_.bindAll(this, 'appendChatConsole', 'disconnect');

			this.template = $('script#message_pane');

			this.model.on('msg_received', this.onMessageReceive, this);
			this.model.on('open', this.onOpen, this);
		},
		render:function (kati, allo) {
			var html = this.template.tmpl(/*this.model.toJSON()*/);
			this.$el.html(html);

			return this;
		},
		makeDialog:function () {
			this.$el.dialog({
				'width':600,
				'height':400,
				'title':'Talking with ' + this.model.id,
				'autoOpen':false,
				'draggable':true,
				'resizable':false,
				'modal':false,
				'buttons':[
					{
						text:'disconnect',
						click:this.disconnect
					}
				]
			});
		},
		onOpen:function (event, data) {
			this.$el.dialog('open');
		},
		disconnect:function () {
			this.remove();

			Mainstream.conn.sochat.announceChat(
				this.model.get('parentDimension').getChatroomJid(),
				Mainstream.getNode(),
				this.model.id,
				'disconnection'
			);

			this.model.collection.remove(this.model);
		},
		onSendButtonClick:function (event, data) {
			event.preventDefault(); //recommended for click actions

			var msg_box = this.$('.message_box');

			var msg_body = msg_box.val();

			if (msg_body === '') {
				//todo throw a warning message to the user
				console.log('empty message is invalid');
			}

			this.model.sendMessage(msg_body);

			this.appendChatConsole(Mainstream.getNode(), msg_body);

			msg_box.val(null);	//clear message box
		},
		appendChatConsole:function (sender, message_body) {
			//todo maybe a new view for the chat console
			var chat_console = this.$('.chat_console');

			var is_at_bottom = Generic.isAtBottom(chat_console);

			//don't need a model for the message view because the data are not supposed to change
			var msgView = new MessageView();
			chat_console.append(msgView.render(sender, message_body).$el);

			if (is_at_bottom) {
				Generic.setToMaxScroll(chat_console);
			}
		},
		onMessageReceive:function (json) {
			this.appendChatConsole(json.sender, json.message_body);
		}
	});

	MessagePanes = Backbone.Collection.extend({
		model:MessagePane,
		parentDimension:null,
		initialize:function () {
			//_.bindAll(this,'safeAdd');	//http://stackoverflow.com/questions/7887595/underscore-bindall-preserving-the-this-context
		}
	});

	MessagePanesView = Backbone.View.extend({
		el:'div#message_panes',
		//
		initialize:function (parentDimension) {
			this.collection = new MessagePanes();
			this.collection.parentDimension = parentDimension;

			this.collection.on('add', this.add_msg_pane, this);
			this.collection.on('remove', this.remove_msg_pane, this);
		},

		add_msg_pane:function (model) {
			model.set('parentDimension', this.collection.parentDimension);

			var modelView = new MessagePaneView({model:model});
			this.$el.append(modelView.render().$el);
			modelView.makeDialog();
		},
		remove_msg_pane:function (model) {
		},
		render:function () {
			return this;
		}
	});
});