/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/19/12
 * Time: 7:13 PM
 * To change this template use File | Settings | File Templates.
 */
$(function () {
	var myScrollable = new ScrollableModel();
	var myScrollView = new ScrollableView({
		model:myScrollable,
		el:'div#the_black_box'
	});

	myScrollView.render();

	var TestModel = Backbone.Model.extend({
		defaults:{
			attr:50
		},
		initialize:function () {

		}
	});

	var myTestModel = new TestModel();
	myTestModel.id = new Array(1, 3);

	console.log(myTestModel);

	var myTestModel2 = new TestModel();
	myTestModel2.id = new Array(4, 3);

	var spring_id = new Array(1,3);
	var reverse_id = spring_id.clone().reverse();

	//console.log(spring_id);
	//console.log(reverse_id);

	var list = new Array(myTestModel, myTestModel2);

	var spring_exists = false;
	_.all(list, function (model) {
		if (spring_id.compareArrays(model.id) || reverse_id.compareArrays(model.id)) {
			spring_exists = true;
			return false;	//break
		}
		return true;	//continue
	}, this);

	console.log(spring_exists);

	var springExists = !_.all(list,function(model){
		return !(spring_id.compareArrays(model.id) || reverse_id.compareArrays(model.id));
//		console.log(spring_id);
//		console.log(reverse_id);
//		console.log(model.id);
//		console.log(res);
	});

	console.log(springExists);

	var obj1 = {
		hello: "hello",
		position: 3
	}

	var obj2 = {
		position: 4
	}

	var obj3 = jQuery.extend(true, {}, obj1, obj2);

	console.log(obj3);
	console.log(obj3 == obj1)
});