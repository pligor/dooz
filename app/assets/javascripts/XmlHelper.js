/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/18/12
 * Time: 3:56 AM
 * To change this template use File | Settings | File Templates.
 */
var XmlHelper = {
    pretty_xml:function (xml, level) {
        var result = [];
        if (!level) {
            level = 0;
        }

        result.push('<div class="xml_level' + level + '">');
        result.push('<span class="xml_punc">&lt;</span>');
        result.push('<span class="xml_tag">');
        result.push(xml.tagName);
        result.push('</span>');

        //attributes
        var attrs = xml.attributes;
        var attr_lead = [];
        for (var i = 0; i < xml.tagName.length + 1; i++) {
            attr_lead.push('&nbsp;');
        }
        attr_lead = attr_lead.join('');

        for (var i = 0; i < attrs.length; i++) {
            result.push(' <span class="xml_aname">');
            result.push(attrs[i].nodeName);
            result.push('</span>');
            result.push('<span class="xml_punc">="</span>');
            result.push('<span class="xml_avalue">');
            result.push(attrs[i].nodeValue);
            result.push('</span>');
            result.push('<span class="xml_punc">"</span>');

            if (i !== (attrs.length - 1)) { //in case we are not at the last element
                result.push('</div>');
                result.push('<div class="xml_level' + level + "'>");
                result.push(attr_lead);
            }
        }

        if (xml.childNodes.length === 0) {   //last element
            result.push('<span class="xml_punc">/&gt;</span></div>');
        }
        else {
            result.push('<span class="xml_punc">&gt;</span></div>');

            //for each children
            $(xml.childNodes).each(function () {
                if (this.nodeType === 1) {
                    result.push(XmlHelper.pretty_xml(this, level + 1));
                }
                else if (this.nodeType === 3) {
                    result.push('<div class="xml_text xml_level' + (level + 1) + '">');
                    result.push(this.nodeValue);
                    result.push('</div>');
                }
            });

            result.push('<div class="xml_level' + level + '">');
            result.push('<span class="xml_punc">&lt;/</span>');
            result.push('<span class="xml_tag">');
            result.push(xml.tagName);
            result.push('</span>');
            result.push('<span class="xml_punc">&gt;</span>');
            result.push('</div>');
        }

        return result.join('');
    },
    xml2html:function (s) {
        return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/, '&gt;');
    },
    text2xml:function (text) {
        var doc = null;
        if (window['DOMParser']) {
            var parser = new DOMParser();
            doc = parser.parseFromString(text, 'text/xml');
        }
        else if (window['ActiveXObject']) {
            doc = new ActiveXObject("MSXML2.DOMDocument");
            doc.async = false;
            doc.loadXML(text);
        }
        else {
            throw {type:'Error from XmlHelper.js', message:'No DOMParser object found'};
        }

        var elem = doc.documentElement;
        if ($(elem).filter('parsererror').length > 0) {
            throw {type:'Error from XmlHelper.js', message:'Parser Error'};
        }

        return elem;
    }
}