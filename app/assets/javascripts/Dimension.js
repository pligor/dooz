/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/18/12
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */
var DimensionModel;

$(function () {
	DimensionModel = Backbone.Model.extend({
		urlRoot:'/dimension',
		defaults:{
			roomName:null,
			canvas:null,
			roster:null,
			msgPanes:null
		},
		initialize:function () {
			_.bindAll(this, 'onPresence', 'onMessage', 'joinUser');

			this.set('msgPanes', new MessagePanesView(this));
			this.set('canvas', new CanvasView(this));
			this.set('roster', typeof(MucRosterView) != 'undefined' ? new MucRosterView(this) : null);	//disablable :P

			this.on('connected', this.joinUser);	//TODO immediate join, maybe have a button later for user to connect/dis

			this.on('directMessage', this.messageExchanged);
		},
		getChatroomJid:function () {
			return this.get('roomName') + '@' + Mainstream.GROUP_CHAT_SERVICE();
		},
		messageExchanged:function (event, data) {
			var targetName = event.username;//twra giati den leitourgei to data akrivws, mallon ta perierga tou backbone pou den moiazei me to jquery

			if (Mainstream.getNode() === targetName) {
				alert('talking with yourself is not very social (:');
				return;
			}

			try {
				this.get('msgPanes').collection.add({
					id:targetName
				});

				//REDUNDANT: EDW EXOUME ENA THEMA OTI THA EKTELEITAI KAI STON PARALIPTI
				//Efoson ginetai announce se olous tha ginetai KAI ston paralipti
				//Pane loipon oloi na prosthesoun to connection, to idio prospathei kai o paraliptis
				//alla diapistwnei oti hdh to exei, ara aplws to agnoei..
				Mainstream.conn.sochat.announceChat(
					this.getChatroomJid(),
					Mainstream.getNode(),
					targetName,
					'connection'
				);
			}
			catch (e) {
				console.log('adding a message pane which already exists has been ignored.' +
					'Error name: ' + e.name + '. Error message: ' + e.message);
			}

			var messagePane = this.get('msgPanes').collection.get(targetName);

			messagePane.trigger('open');
		},
		joinUser:function () {
			Mainstream.conn.muc.join(
				this.getChatroomJid(), //room
				Mainstream.getNode(), //nick
				this.onMessage, //msg_handler_cb
				this.onPresence, //pres_handler_cb
				null //password
			);
		},
		onMessage:function (msg) {
			//console.log(msg);
			msg = $(msg);

			var messageType = msg.attr('type');

			var msg_body;
			if (messageType === 'chat') {
				var from = msg.attr('from');
				var source_name = Strophe.getResourceFromJid(from);

				msg_body = msg.children('body').text();

				this.trigger('directMessage', {username:source_name});

				var msg_pane = this.get('msgPanes').collection.get(source_name);	//MessagePane model
				msg_pane.trigger('msg_received', {sender:source_name, message_body:msg_body});
			}
			else if (messageType === 'groupchat') {
				if (Mainstream.conn.sochat.isAnnounceChat(msg)) {
					msg_body = msg.children('body').text();

					$('div#chatroom').append('<div>' + msg_body + '</div>');	//TODO only for debugging purposes
					var chatfrom = msg.children('chatfrom').text();
					var chatto = msg.children('chatto').text();
					var type = msg.children('type').text();

					var start_node = this.get('canvas').model.get('nodes').get(chatfrom);
					var end_node = this.get('canvas').model.get('nodes').get(chatto);

					if (type === 'connection') {
						start_node.connectWith(end_node);
					}
					else if (type === 'disconnection') {
						start_node.disconnectWith(end_node);
					}
				}
				else {
					//var msg_body = msg.children('body').text();
					//$('div#chatroom').append('<div>'+msg_body+'</div>');
					console.log('other groupchat message');
				}
			}

			//repeat
			return true;
		},
		onPresence:function (pres) {
			console.log(pres);
			pres = $(pres);
			var from = pres.attr('from');

			//var bareJid = Strophe.getBareJidFromJid(from);
			var username = Strophe.getResourceFromJid(from);

			var presType = pres.attr('type');

			if (presType === undefined) {
				/* sample:
				 *	<presence xmlns="jabber:client" from="first.and.only.room@conference.127.0.0.1/boy" to="boy@127.0.0.1/7b505a37">
				 *		<x xmlns="http://jabber.org/protocol/muc#user">
				 *			<item affiliation="none" role="participant"></item>
				 *		</x>
				 *	</presence>
				 */
				var presItem = pres.find('item');

				var affiliation = presItem.attr('affiliation');
				var role = presItem.attr('role');

				this.userInDimension(username, affiliation, role);
			}
			else if (presType === 'unavailable') {
				this.userOutOfDimension(username);
			}
			else if (presType === 'error') {
				//DO NOTHING because of this:
				//http://xmpp.org/extensions/xep-0206.html#recipient-unavailable
			}
			else {
				console.log('inside onPresence: other kind of unexpected presence');
				throw 'inside onPresence: other kind of unexpected presence';
			}

			//repeat
			return true;
		},
		userInDimension:function (username, affiliation, role) {
			this.trigger('userInDimension', {
				username:username,
				affiliation:affiliation,
				role:role
			});
			/*this.get('roster').collection.add({
			 id:username,
			 affiliation:affiliation,
			 role:role
			 });*/

			$.ajax({    //http://api.jquery.com/jQuery.ajax/
				url:"/dimension/" + this.id + "/position/" + username,
				data:{
					//empty
				},
				method:'GET',
				context:this, //yes indeed this is the outer object! here is the Dimension
				success:function (response) {
//documentation: Any event that is triggered on a model in a collection will also be triggered on the collection directly
					//console.log(response);
					this.get('canvas').model.get('nodes').add({
						id:username,
						position:Vector.create([response.x, response.y])
					});
				},
				error:function () {
					alert("ajax failed");
				}
			});
		},
		userOutOfDimension:function (username) {
			this.trigger('userOutOfDimension', {
				username:username
			});
			/*var rosterCollection = this.get('roster').collection;
			 rosterCollection.remove(rosterCollection.get(username));*/

			var nodeCollection = this.get('canvas').model.get('nodes');

			try {
				nodeCollection.remove(nodeCollection.get(username));
			}
			catch (e) {
				console.log(e);
			}
		}
	});
})