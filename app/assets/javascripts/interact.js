/**
 * Created by pligor
 */
var Dimension;

$(function () {
	Mainstream.XMPP_DOMAIN = xmppDomain;
	console.log('you have selected credentials: ' + user + ';' + pass);
	$(document).trigger("connect", {user:user, pass:pass});

	Dimension = new DimensionModel(dimensionAttributes);
	Dimension.get('canvas').render();

	//console.log(Dimension.url());
	//Dimension.fetch();	//fetch should only be used for lazy loading !!
	console.log(Dimension.getChatroomJid());

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 Dimension.get('canvas').paper.circle(100, 100, 60);

	 var position = Dimension.get('canvas').model.getRandomPosition();
	 var node1 = Dimension.get('canvas').model.get('nodes').add({
	 id:'filos',
	 position:position
	 });

	 position = Dimension.get('canvas').model.getRandomPosition();
	 var node2 = Dimension.get('canvas').model.get('nodes').add({
	 id:'filh',
	 position:position
	 });

	 Dimension.get('roster').collection.add({
	 id:'beautiful'
	 });

	 Dimension.get('msgPanes').collection.add({
	 target_name:"dreamgirl"
	 });
	 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$('#test1').click(function (event, data) {
		event.preventDefault();	//recommended for click events
		node1.connectWith(node2);
		node2.connectWith(node1);
		return false;
	});

	$('#test2').click(function (event, data) {
		event.preventDefault();	//recommended for click events

		Dimension.get('canvas').model.get('nodes').shuffleNodes();
		return false;
	});

	$('#test3').click(function (event, data) {
		event.preventDefault();	//recommended for click events
		Dimension.get('roster').collection.remove(Dimension.get('roster').collection.get('beautiful'));
		return false;
	});

	$('#test4').click(function (event, data) {
		event.preventDefault();	//recommended for click events
		var msgPanes = Dimension.get('msgPanes');
		console.log(msgPanes);
		console.log('message panes length: ' + msgPanes.collection.length);
		return false;
	});
	 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	*/
});