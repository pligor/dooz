/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/17/12
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
Peek_vtwo = Backbone.Model.extend({
	defaults:{
		body:null, //this could be a collection or an array but never mind for now :P
		message:null,
		conn:null
	},
	initialize:function () {
		//console.log("to model ginetai initialize");
		_.bindAll(this, 'showTraffic');

		this.on('connection_set', function (connection) { //do NOT set function(event, data) as parameters for a backbone event callback
			this.conn = connection;
			this.conn.xmlInput = _.bind(function (data) {
				//console.log("incoming!")
				this.showTraffic(data, 'incoming');
			}, this);
			this.conn.xmlOutput = _.bind(function (data) {
				//console.log("outgoing!!")
				this.showTraffic(data, 'outgoing');
			}, this);
		});
	},
	showTraffic:function (data, type) {
		if (data.childNodes.length <= 0) {
			return;
		}

		var styling = (type == 'incoming') ? 'style="background-color: #DDD;"' : null;

		var nodes = $(data.childNodes).map(function (index, domElement) {
			//Within the callback function, this refers to the current DOM element for each iteratio
			return '<div ' + styling + '>' +
				//Peek.xml2html(Strophe.serialize(this))
				XmlHelper.pretty_xml(this)
				+ '</div>';
		});

		nodes.each(_.bind(function (index, Element) {
			//the keyword "this" refered to the element but we changed it with _.bind to refer to the model
			this.set({
				body:this.get('body') + Element    //append it
			});

			//a hack (see also view)
			this.trigger('body_changed', this, Element);

		}, this));
	}
})

Peek_vtwoView = Backbone.View.extend({
	el:'div#peek_dialog',
	//model: Peek_vtwo,
	events:{ //SOS: It might not be obvious, but event property in backbone.js views is used only for DOM events
		//'click a': 'clicked'
		//'change' : 'changed'
		'keyup #peek_input':'keyClicked',
		'click #send_button':'sendButtonClicked'//,
		//'change:body':'bodyChanged'
	},
	//itsTmpl: null,
	initialize:function () {
		//this.itsTmpl = $('script#input_tmpl');		//remember to include template script in final html

		//I guess obviously the other functions like "render" or
		// defined from events like "keyClicked" are already bind to this object :P
		_.bindAll(this, 'setInputState', 'disableInput', 'enableInput', 'bodyChanged');
		this.model.on('connected', this.enableInput);
		this.model.on('disconnected', this.disableInput);

		//this.model.on('change:body', this.bodyChanged);
		this.model.on('body_changed', this.bodyChanged); //a hack to avoid creating a whole collection
	},
	bodyChanged:function (model, value) {
		var peekConsole = this.$('#peek_console');

		var isAtBottom = Generic.isAtBottom(peekConsole);

		peekConsole.append(value);

		if (isAtBottom) {
			Generic.setToMaxScroll(peekConsole);
		}
	},
	sendButtonClicked:function () {
		//console.log("ok I am clicked")
		var input = this.model.get('message');

		try {
			var xml_or_builder = (input[0] === '<') ?
				XmlHelper.text2xml(input) : (
				(input[0] === '$') ?
					eval(input) :
					null
				)
			console.log(xml_or_builder);
			if (xml_or_builder == null) {
				throw {type:'Error in input', message:'Input must begin with < or $'}
			}

			this.model.conn.send(xml_or_builder);
			this.keyClicked();
		}
		catch (e) {
			this.$('#peek_input').css({'background-color':'#aaa'});
			console.log('Exception: ' + e.message);
		}
	},
	keyClicked:function () {
		//when the change comes from the view itself is only logical that we want it in a silent way!
		var peekInput = this.$('#peek_input');
		this.model.set({
			message:peekInput.val()
		}, {
			silent:true
		});

		this.$('#send_button').attr('disabled', this.model.get("message") == '');
		peekInput.css({'background-color':'#fff'}); //ok this line I don't remember why I put it :P
	},
	enableInput:function () {
		this.setInputState(false);
	},
	disableInput:function () {
		this.setInputState(true);
	},
	setInputState:function (disabled) {
		this.$('#peek_input').attr('disabled', disabled); //each view has a $ function that runs queries scoped within the view's element
		//console.log(this);
	},
	render:function () {
		var el = $(this.el);
		//$("#peek_dialog").dialog({
		el.dialog({
			width:900,
			height:700,
			title:"Peek",
			autoOpen:false,
			draggable:true,
			modal:false
		});

		return this;
	}
})