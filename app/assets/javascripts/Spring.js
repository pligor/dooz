/**
 * Created by pligor
 */
var SpringModel;
var SpringView;
var SpringCollection;

$(function () {

	SpringModel = Backbone.Model.extend({
		defaults:{
			start_node:null,
			end_node:null
		},
		initialize:function () {
			//note that "on" takes different parameters in Backbone: object.on(event, callback, [context])
			this.on('remove', this.removed, this);
		},
		convPoints2linePath:function () {
			var start_point = this.get('start_node').get('position').e(1) + ',' + this.get('start_node').get('position').e(2);
			var end_point = this.get('end_node').get('position').e(1) + ',' + this.get('end_node').get('position').e(2);
			return 'M' + start_point + 'L' + end_point;
		},
		equalPoints:function (spring) {    //direction does NOT matter
			return (
				(this.get('start_node') === spring.get('start_node')) && //same spring
					(this.get('end_node') === spring.get('end_node'))
				) || (
				(this.get('start_node') === spring.get('end_node')) && //same reverse spring
					(this.get('end_node') === spring.get('start_node'))
				);
		},
		removed:function () {
			this.get('start_node').get('springs').remove(this);
			this.get('end_node').get('springs').remove(this);
		}
	});

	SpringView = Backbone.View.extend({
		parent:null,
		shape:null,
		initialize:function () {
			this.model.on('nodeMoved', this.redraw, this);
			this.model.on('remove', this.removed, this);
		},
		render:function () {
			this.shape = this.parent.paper.path(this.model.convPoints2linePath());
			return this;
		},
		redraw:function () {
			this.shape.attr('path', this.model.convPoints2linePath());
		},
		removed:function () {
			try {
				this.shape.remove();
			}
			catch (e) {
				console.log(e);
			}
		}
	});

	SpringCollection = Backbone.Collection.extend({
		model:SpringModel,
		parentNodeModel:null,
		autoincrement:1,
		initialize:function () {
			_.bindAll(this, 'springExists');	//http://stackoverflow.com/questions/7887595/underscore-bindall-preserving-the-this-context
			this.on('node_moved', this.nodeMoved, this);
		},
		/**
		 * @static
		 * @param obj
		 * @return {*}
		 */
		generateID:function (obj) {    //here we get a composite key
			var json;	//object can be plain json model OR real backbone model. help here: http://stackoverflow.com/questions/2481614/how-do-you-know-if-an-object-is-json-in-javascript
			try {
				json = obj.toJSON();
			}
			catch (e) {
				json = obj;
			}

//			console.log(obj);
//			console.log(json);
//			console.trace();
			//throw "edw";

			json.id = new Array(json.start_node.id, json.end_node.id);

			return json.id;
		},
		add:function (value) {
			//console.log(obj);
			//console.trace();
			var createId = _.bind(function (obj) {
				obj.id = this.generateID(obj);

				if (this.springExists(obj.id)) {
					throw 'spring already exists in the collection with parent node with id: ' + this.parentNodeModel.id;
				}
			}, this);

			if (jQuery.isArray(value)) {
				//throw 'we have disabled array addition for this collection. please add each model one at a time';
				_.each(value, function (obj, index, list) {
					createId(obj);
				});
			}
			else {
				createId(value);
			}

			Backbone.Collection.prototype.add.call(this, value);	//call parent's "add" method
			//return this.get(obj.id);	//normally add does not return any value, we would like to get it though
		},
		nodeMoved:function () {
			this.each(function (model, index, list) {
				model.trigger('nodeMoved');	//must be named differently because the event is propagated back to the collection
			});
		},
		/**
		 * @param spring_id
		 * @return {Boolean}
		 */
		springExists:function (spring_id) {
			var reverse_id = spring_id.clone(); //clone array
			reverse_id.reverse();

			return !this.all(function (model) {
				return !(spring_id.compareArrays(model.id) || reverse_id.compareArrays(model.id))
			});
		},
		clear:function () {
			this.each(function (model, index, list) {
				model.trigger('remove');
				//TODO des an mporoume auto na to kanoume me to "reset" event
			});
			this.reset();
		}
	});
});