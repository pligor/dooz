/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/18/12
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */
var Canvas;
var CanvasView;

$(function () {
	Canvas = Backbone.Model.extend({
		//Remember: in javascript objects are passed by reference so an object is shared among all models
		defaults:{
			width:960,
			height:540,
			//backcolor:'rgb(180, 150, 100)',
			backcolor:'#FFFAF0',
			nodes:null,
			springs:null,
			parentDimension:null
		},
		initialize:function () {
			var nodeCollection = new NodeCollection();
			nodeCollection.parent = this;	//store the parent permenantly in the collection so all the nodes in the collection can get it
			this.set('nodes', nodeCollection);

			this.on('directMessage', function (event, data) {    //just forward message
				var username = event.username;	//twra giati den leitourgei edw akrivws, mallon ta perierga tou backbone pou den moiazei me to jquery
				this.get('parentDimension').trigger('directMessage', {username:username});
			}, this);
		},
		getRandomPosition:function () {
			var x = mt_rand(0, this.get('width'));
			var y = mt_rand(0, this.get('height'));
			return Vector.create([x, y]);
		}
	});

	CanvasView = Backbone.View.extend({
		el:'div#canvas_container',
		paper:null,
		background:null,
		nodeViews:null,
		initialize:function (parentDimension, width, height) {
			this.model = ((width != undefined) && (height != undefined)) ?
				new Canvas({
					width:width,
					height:height
				})
				:
				new Canvas();
			this.model.set('parentDimension', parentDimension);

			this.nodeViews = new NodeCollectionView({
				collection:this.model.get('nodes')
			});
			this.nodeViews.parent = this;	//store the parent permenantely so all the children node views can have access to the paper!
		},
		render:function () {
			try {
				this.paper = new Raphael(
					this.$el.attr('id'),
					this.model.get('width'),
					this.model.get('height')
				);
				this.background = this.paper.rect(0, 0, this.model.get('width'), this.model.get('height'));
				this.background.attr('fill', this.model.get('backcolor'));
			}
			catch (e) {
				console.log('Error name: ' + e.name + '. Error message: ' + e.message);
			}

			return this;
		}
	});
});