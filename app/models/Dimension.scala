package models

import play.api._;
import db.DB
import anorm._;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Date: 9/23/12
 * Time: 3:55 PM
 * To change this template use File | Settings | File Templates.
 */
class Dimension(id: Int, name: String, width: Int, height: Int) {

}

object Dimension {
  def newCoords(width: Int, height: Int): (Int, Int) = {
    //(1,1)
    ///*
    //TODO to avoid var we must use field parsers like in this link:
    //http://woss.name/2012/04/02/retrieving-bigdecimals-from-a-database-with-anorm-scala/
    var newCoords: (Int, Int) = (0, 0);

    import play.api.Play.current;

    DB.withConnection {
      implicit c =>
        val sqlString = "SELECT x,y FROM dimension_position";

        val sqlQuery = SQL(sqlString);

        val vectors: List[(Int, Int)] = sqlQuery().map(row =>
          row[Int]("x") -> row[Int]("y")
        ).toList;

        //val vectors = List[(Int, Int)](0 -> 1, 0 -> 3, 3 -> 2);

        import components.Math._;

        def forever: (Int, Int) = {
          val newVector: (Int, Int) = (rand(0, width - 1), rand(0, height - 1));
          if (isAcceptable(newVector, vectors)) {
            Logger info "got it";
            newVector;
          }
          else {
            Logger info "pame";
            forever;
          }
        }

        newCoords = forever;
    }

    newCoords;
    //*/
  }

  private def isAcceptable(newVector: (Int, Int), vectors: List[(Int, Int)]): Boolean = {
    //vectors.forall(vector => vector != newVector);
    vectors.forall(vector => distance(vector,newVector) > 2*20 ); //TODO depend radius on database both for scala and javascript
  }

  private def distance(vector1: (Int, Int), vector2: (Int, Int)): Double = {
    //subtract
    val subtracted = (vector1._1 - vector2._1, vector1._2 - vector2._2);
    //modulus
    import scala.math._;
    sqrt(pow(subtracted._1, 2) + pow(subtracted._2, 2));
  }
}
