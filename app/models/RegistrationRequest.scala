package models

import java.sql.Date

/**
 * Created by pligor
 */
class RegistrationRequest(id: Int, nickname: String, email: String, time_requested: Date) {
  //for the time check this: http://dev.mysql.com/doc/refman/5.0/en/connector-j-reference-type-conversions.html
  //AND this: http://dev.mysql.com/doc/refman/5.0/en/timestamp-initialization.html
  //and TODO to check the above in server. Check if the sql: SELECT NOW(); returns the same time as scala

}