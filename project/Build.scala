import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

  val appName = "dooz"
  val appVersion = "0.3-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    //group % artifact % revision (or double % to ask for compatible version, in the special cases where there is
    // a library for each scala version)
    "mysql" % "mysql-connector-java" % "5.1.21", //comment if you upload to heroku, not sure how to use mysql yet
    "org.scalatest" %% "scalatest" % "1.8"
  )

  val main = PlayProject(appName, appVersion, appDependencies, mainLang = SCALA).settings(
    // Add your own project settings here
  )

}
